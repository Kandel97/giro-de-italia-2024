from django.db import models

from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse

# Create your models here.
# Creo una clase tipo Tean que hereda de Model que es una clase de Django 
class Team(models.Model):
    """ equipo  """
    #el quipo tiene: nombre, escudo, equipo, fecha
    # tipo cadena de caracteres con una longitud de 50
    name = models.CharField(max_length=50)
    # escudo crea un campo de tipo imagen que se cuarda en la carpeta shields
    shield = models.ImageField(upload_to='shields/')
    # el equipo de tipo imagen
    team = models.ImageField(upload_to='teams/')
    # fecha cuando creo un equipo automaticamente pone la fecha de creación
    pub_date = models.DateField(auto_now_add=True)

    # Sirve para que cuando llame un equipo me devuelve el nombre
    def __str__(self):
        return self.name
# la clase ciclista hereda de model
class Player(models.Model):
    """ ciclista """
    #equipo al que pertenece y es una llave foranea que apunta a la tabla Team en BD
    # PROTECTED realiza la alerta para que no borre cosas de la bd
    team = models.ForeignKey('Team', on_delete=models.PROTECT,related_name='get_players' )
    # Datos de los ciclistas
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='players/')
    pub_date = models.DateField(auto_now_add=True)
    height = models.DecimalField(max_digits=3, decimal_places=2)
    weight = models.IntegerField()
    #cuando ponemos blank= TRue el campo es obligatorio
    comment = models.CharField(max_length=200, blank=True)
    #Me devuelve el primer nombre y apellido del ciclista
    def __str__(self):
        return self.first_name + " " + self.last_name
    
    def get_absolute_url(self):
        return reverse('player-list')